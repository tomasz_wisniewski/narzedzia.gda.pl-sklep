<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{blockadvertising}prestashop>blockadvertising_bedd646b07e65f588b06f275bd47be07'] = 'Blok reklam';
$_MODULE['<{blockadvertising}prestashop>blockadvertising_dc23a0e1424eda56d0700f7ebe628c78'] = 'Dodaje blok z reklamami w wybranych sekcjach Twojego sklepu.';
$_MODULE['<{blockadvertising}prestashop>blockadvertising_b15e7271053fe9dd22d80db100179085'] = 'Ten moduł powinien być umieszczony w kolumnie, a Twój szablon ich nie posiada';
$_MODULE['<{blockadvertising}prestashop>blockadvertising_070e16b4f77b90e802f789b5be583cfa'] = 'Błąd wczytywania pliku.';
$_MODULE['<{blockadvertising}prestashop>blockadvertising_254f642527b45bc260048e30704edb39'] = 'Konfiguracja';
$_MODULE['<{blockadvertising}prestashop>blockadvertising_89ca5c48bbc6b7a648a5c1996767484c'] = 'Blok zdjęcia';
$_MODULE['<{blockadvertising}prestashop>blockadvertising_05177d68c546e5b754e39ae3ce211cc2'] = 'Grafika zostanie wyświetlona w rozmiarze 155 na 163 pikseli.';
$_MODULE['<{blockadvertising}prestashop>blockadvertising_9ce38727cff004a058021a6c7351a74a'] = 'Adres obrazka';
$_MODULE['<{blockadvertising}prestashop>blockadvertising_b78a3223503896721cca1303f776159b'] = 'Tytuł';
$_MODULE['<{blockadvertising}prestashop>blockadvertising_c9cc8cce247e49bae79f15173ce97354'] = 'Zapisz';


return $_MODULE;
