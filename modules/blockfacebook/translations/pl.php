<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{blockfacebook}prestashop>blockfacebook_06d770829707bb481258791ca979bd2a'] = 'Blok Facebooka';
$_MODULE['<{blockfacebook}prestashop>blockfacebook_f66e16efd2f1f43944e835aa527f9da8'] = 'Wyświetla blok subskrypcji Twojej strony na Facebooku.';
$_MODULE['<{blockfacebook}prestashop>blockfacebook_20015706a8cbd457cbb6ea3e7d5dc9b3'] = 'Konfiguracja zaktualizowana';
$_MODULE['<{blockfacebook}prestashop>blockfacebook_f4f70727dc34561dfde1a3c529b6205c'] = 'Ustawienia';
$_MODULE['<{blockfacebook}prestashop>blockfacebook_c98cf18081245e342d7929c117066f0b'] = 'Link do Facebook\'a (podaj pełny URL)';
$_MODULE['<{blockfacebook}prestashop>blockfacebook_c9cc8cce247e49bae79f15173ce97354'] = 'Zapisz';
$_MODULE['<{blockfacebook}prestashop>blockfacebook_a4aa9244ea53b5d916dfd8f024d11e22'] = 'Śledź nas na Facebooku';
$_MODULE['<{blockfacebook}prestashop>preview_31fde7b05ac8952dacf4af8a704074ec'] = 'Podgląd';
$_MODULE['<{blockfacebook}prestashop>preview_a4aa9244ea53b5d916dfd8f024d11e22'] = 'Śledź nas na Facebooku';


return $_MODULE;
