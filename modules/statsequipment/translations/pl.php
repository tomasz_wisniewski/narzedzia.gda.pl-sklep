<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{statsequipment}prestashop>statsequipment_247270d410e2b9de01814b82111becda'] = 'Przeglądarki i systemy operacyjne';
$_MODULE['<{statsequipment}prestashop>statsequipment_2876718a648dea03aaafd4b5a63b1efe'] = 'Dodaje zakładkę zawierającą wykresy dotyczące przeglądarki i użytkowania systemu operacyjnego do statystyki na tablicy.';
$_MODULE['<{statsequipment}prestashop>statsequipment_6602bbeb2956c035fb4cb5e844a4861b'] = 'Poradnik';
$_MODULE['<{statsequipment}prestashop>statsequipment_955114028663527c20093ff65028f47d'] = 'Upewnij się, że Twoja strona jest dostępna dla wszystkich';
$_MODULE['<{statsequipment}prestashop>statsequipment_2d7bfb32c74f1ff68db268d409b71cb9'] = 'Podczas zarządzania Strony, ważne jest, aby śledzić oprogramowania wykorzystywanego przez użytkowników, aby upewnić się, że strona wyświetla się tak samo dla każdego. PrestaShop został zbudowany, aby być zgodne z najnowszymi przeglądarek internetowych i systemów operacyjnych (OS) komputera. Jednakże, ponieważ może skończyć się dodawanie zaawansowanych funkcji do witryny, a nawet modyfikować kod rdzenia PrestaShop, te dodatki nie mogą być dostępne dla każdego. Dlatego jest to dobry pomysł, aby mieć oko na odsetek użytkowników dla każdego rodzaju oprogramowania przed dodanie lub zmianę coś, że tylko ograniczona liczba użytkowników będzie w stanie przejść.';
$_MODULE['<{statsequipment}prestashop>statsequipment_d36312e9992d0b03780a32fedd6650b7'] = 'Określ podział procentowy przeglądarek używanych przez Twoich klientów.';
$_MODULE['<{statsequipment}prestashop>statsequipment_998e4c5c80f27dec552e99dfed34889a'] = 'Export CSV';
$_MODULE['<{statsequipment}prestashop>statsequipment_c560add3373d03ea2723069fb428719a'] = 'Procentowe określenie systemów operacyjnych używanych przez klientów';
$_MODULE['<{statsequipment}prestashop>statsequipment_bb38096ab39160dc20d44f3ea6b44507'] = 'Wtyczki (Plug-ins)';
$_MODULE['<{statsequipment}prestashop>statsequipment_ef83f5147398f2735fddfaac983983d7'] = 'Wykorzystane przeglądarki internetowe';
$_MODULE['<{statsequipment}prestashop>statsequipment_0a66f998cfad4890a14c9b9a1df8deb3'] = 'Wykorzystywane systemy operacyjne';


return $_MODULE;
